package engineer.fernando.cms.tracks.actordb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TrackActorDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrackActorDatabaseApplication.class, args);
	}

}
