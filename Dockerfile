# the first stage of our build will extract the layers
FROM adoptopenjdk:14-jre-hotspot as builder
LABEL stage=builder
WORKDIR application
COPY target/TrackActorDatabase-1.1.jar TrackActorDatabase-1.1.jar
RUN java -Djarmode=layertools -jar TrackActorDatabase-1.1.jar extract

# the second stage of our build will copy the extracted layers
FROM adoptopenjdk:14-jre-hotspot
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
